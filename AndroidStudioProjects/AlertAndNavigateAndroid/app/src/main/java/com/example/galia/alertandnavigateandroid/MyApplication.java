package com.example.galia.alertandnavigateandroid;

import android.app.Application;
import android.content.Context;

/**
 * Created by Galia on 10/07/2018.
 */

public class MyApplication extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}