package com.example.galia.sheltersmanagement.Controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.galia.sheltersmanagement.Model.Model;
import com.example.galia.sheltersmanagement.Model.Shelter;
import com.example.galia.sheltersmanagement.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Galia on 12/07/2018.
 */


public class NewShelterFragment extends Fragment{
    private static final String ARG_ADDRESS = "ARG_ADDRESS";

    public NewShelterFragment() {
        // Required empty public constructor
    }

    EditText addressEt;
    EditText langEt;
    EditText latEt;
    EditText starsEt;
    EditText desEt;
    EditText shelterTypeEt;
    ImageView image;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_shelter, container, false);

        addressEt = view.findViewById(R.id.new_shelter_address);
        langEt = view.findViewById(R.id.new_shelter_lang);
        latEt = view.findViewById(R.id.new_shelter_lat);
        starsEt = view.findViewById(R.id.new_shelter_stars);
        desEt = view.findViewById(R.id.new_shelter_des);
        shelterTypeEt = view.findViewById(R.id.new_shelter_type);
        progress = view.findViewById(R.id.new_shelter_progress);
        progress.setVisibility(View.GONE);
        Button save = view.findViewById(R.id.new_shelter_save);


        //Event click handeling
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress . setVisibility(View.VISIBLE);
                final Shelter sh = new Shelter();
                sh.address = addressEt.getText().toString();
                sh.lang = langEt.getText().toString();
                sh.lat = latEt.getText().toString();
                sh.name = addressEt.getText().toString();
                try {
                    sh.stars = Double.parseDouble(starsEt.getText().toString());
                } catch(NumberFormatException nfe) {
                    Log.d("TAG","Error while converting string to double");
                }
                sh.description = desEt.getText().toString();
                sh.shelterType = shelterTypeEt.getText().toString();

                //save image
                if (imageBitmap != null) {
                    Model.instance.saveImage(imageBitmap, new Model.SaveImageListener() {
                        @Override
                        public void onDone(String url) {
                            //save student obj
                            sh.imageUrl = url;
                            Model.instance.addShelter(sh);
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                }
                else{
                            sh.imageUrl = "";
                            Model.instance.addShelter(sh);
                            getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });


        Button cancel = view.findViewById(R.id.new_shelter_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        if (savedInstanceState != null) {
            String address = savedInstanceState.getString(ARG_ADDRESS);
            if (address != null) {
                addressEt.setText(address);
            }
        }

        Button editImage = view.findViewById(R.id.new_shelter_img_btn);
        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //open camera
                Intent takePictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        image = view.findViewById(R.id.new_shelter_image);
        return view;
    }


    Bitmap imageBitmap;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE &&
                resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);
        }
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }
    @Override
    public void  onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);
        bundle.putString(ARG_ADDRESS, addressEt.getText().toString());
    }

}
