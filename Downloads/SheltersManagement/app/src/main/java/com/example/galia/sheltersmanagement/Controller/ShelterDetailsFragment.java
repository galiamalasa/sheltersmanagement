package com.example.galia.sheltersmanagement.Controller;

/**
 * Created by Galia on 11/07/2018.
 */

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.galia.sheltersmanagement.Model.Model;
import com.example.galia.sheltersmanagement.R;

public class ShelterDetailsFragment extends Fragment{

    TextView addressEt;
    TextView langEt;
    TextView latEt;
    TextView starsEt;
    TextView desEt;
    TextView shelterTypeEt;
    ImageView imageView;


    public ShelterDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shelter_details, container, false);

        addressEt = view.findViewById(R.id.d_shelter_address);
        langEt = view.findViewById(R.id.d_shelter_lang);
        latEt = view.findViewById(R.id.d_shelter_lat);
        starsEt = view.findViewById(R.id.d_shelter_stars);
        desEt = view.findViewById(R.id.d_shelter_des);
        shelterTypeEt = view.findViewById(R.id.d_shelter_type);
        imageView = view.findViewById(R.id.d_shelter_image);

        final String address = getArguments().getString("address");
        String lang = getArguments().getString("lang");
        String lat = getArguments().getString("lat");
        String stars = getArguments().getString("stars");
        String des = getArguments().getString("des");
        String shelterType = getArguments().getString("type");
        String image = getArguments().getString("image");

        addressEt.setText(address);
        langEt.setText(lang);
        latEt.setText(lat);
        starsEt.setText(stars);
        desEt.setText(des);
        shelterTypeEt.setText(shelterType);

        imageView.setImageResource(R.drawable.home);
        imageView.setTag(address);
        //Checking if image url is not null to convert image into Bitmap
        if (image != null && (image.length() > 2)){
            Model.instance.getImage(image, new Model.GetImageListener() {
                @Override
                public void onDone(Bitmap imageBitmap) {
                    if (address.equals(imageView.getTag()) && imageBitmap != null) {
                        imageView.setImageBitmap(imageBitmap);
                    }
                }
            });
        }


        return view;
    }
}
