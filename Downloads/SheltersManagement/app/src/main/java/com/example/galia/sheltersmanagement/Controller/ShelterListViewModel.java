package com.example.galia.sheltersmanagement.Controller;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.galia.sheltersmanagement.Model.Model;
import com.example.galia.sheltersmanagement.Model.Shelter;

import java.util.List;

/**
 * Created by Galia on 11/07/2018.
 */


/*ViewModel
● Provides the data for a specific UI component
● Handles the communication with the business
part of data handling
● Does not know about the View
● Not affected by configuration changes such as
recreating an activity due to rotation.*/

public class ShelterListViewModel extends ViewModel{
    LiveData<List<Shelter>> data;
    /*
    Live Data is Holder class that can be observed within a given lifecycle.
    The observer will be notified about modifications of the
    wrapped data only if it is in active state (STARTED or
    RESUMED)
     */
    public LiveData<List<Shelter>> getData(){
        data = Model.instance.getAllShelters();
        return data;
    }
}


