package com.example.galia.sheltersmanagement.Controller;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.galia.sheltersmanagement.Model.Model;
import com.example.galia.sheltersmanagement.Model.Shelter;
import com.example.galia.sheltersmanagement.R;

import java.util.List;

/**
 * Created by Galia on 11/07/2018.
 */

public class SheltersListFragment extends Fragment{
    //List which holds the list wiew from Layout : "fragment_shelter_list"
    ListView list;
    //Adapter
    MyAdapter myAdapter = new MyAdapter();
    //The get data object is holder to the live data which observes on shelters list from f.b and local cahe
    ShelterListViewModel dataModel;

    //This function called once the fragment is associated with its activity
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataModel = ViewModelProviders.of(this).get(ShelterListViewModel.class);
        dataModel.getData().observe(this, new Observer<List<Shelter>>() {
            @Override
            public void onChanged(@Nullable List<Shelter> shelters) {
                myAdapter.notifyDataSetChanged();
                Log.d("TAG","notifyDataSetChanged" + shelters.size());
            }
        });
    }

    // Called to do final cleanup of the fragment's state.
    @Override
    public void onDestroy() {
        super.onDestroy();
        Model.instance.cancellGetAllShelters();
    }

    //Creating list view settings using the adapter and live data wrapping objects(ShelterListViewModel)
    //Creates and returns the view hierarchy associated with the fragment  - most important function
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shelters_list, container, false);
    //List view id from fragment layout
        list = view.findViewById(R.id.shelterslist_list);
        //Connecting adapter to our list view
        list.setAdapter(myAdapter);

        //Handeling click on item
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //i index in row
                Log.d("TAG","item selected:" + i);

                ShelterDetailsFragment shelterDetailsFragment = new ShelterDetailsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container, shelterDetailsFragment);
                Bundle args = new Bundle();

                final Shelter s = dataModel.getData().getValue().get(i);
                Bundle arg = new Bundle();
                arg.putString("address",s.address);
                arg.putString("lang",s.lang);
                arg.putString("lat",s.lat);
                arg.putString("stars",s.stars.toString());
                arg.putString("des",s.description);
                arg.putString("type",s.shelterType);
                arg.putString("image",s.imageUrl);
                shelterDetailsFragment.setArguments(arg);

                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return view;
    }

    //Called immediately prior to the fragment no longer being associated with its activity.
    @Override
    public void onDetach() {
        super.onDetach();
    }

    // Adapters are used for binding data to a control
    // Adapters are employed for widgets that
    //    extend android.widget.AdapterView:
    //            ○ ListView

    class MyAdapter extends BaseAdapter {
        public MyAdapter(){
        }

        //This function returns the count of list dataModel count
        @Override
        public int getCount() {
            Log.d("TAG","list size:" + dataModel.getData().getValue().size());
            return dataModel.getData().getValue().size();

        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        //This method defines how each item will be displyed
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            //Reusing
            //Inflater is an object that excepts the layout xml and connects the widgets in this file
            //Reusing cells for displaying
            if (view == null){
                view = LayoutInflater.from(getActivity()).inflate(R.layout.shelter_list_item,null);
            }

            final Shelter s = dataModel.getData().getValue().get(i);
            TextView addressTv = view.findViewById(R.id.shListItem_address_tv);
            final ImageView imageView = view.findViewById(R.id.shListItem_imageUrl);

            addressTv.setText(s.address);
            imageView.setImageResource(R.drawable.home);
            imageView.setTag(s.address);
            //Checking if image url is not null to convert image into Bitmap
            if (s.imageUrl != null && (s.getImageUrl().length() > 2)){
                Model.instance.getImage(s.imageUrl, new Model.GetImageListener() {
                    @Override
                    public void onDone(Bitmap imageBitmap) {
                        if (s.address.equals(imageView.getTag()) && imageBitmap != null) {
                            imageView.setImageBitmap(imageBitmap);
                        }
                    }
                });
            }
            return view;
        }
    }

}
