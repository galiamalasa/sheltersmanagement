package com.example.galia.sheltersmanagement.Model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.example.galia.sheltersmanagement.MyApplication;

/**
 * Created by Galia on 11/07/2018.
 */

//Defining new database for shelters
@Database(entities = {Shelter.class}, version = 1)
abstract class AppLocalDbRepository extends RoomDatabase {
    //Contains the methods used for accessing the database.
    public abstract ShelterDao shelterDao();
}

//At runtime, you can acquire an instance of Database by calling Room.databaseBuilder() or Room.inMemoryDatabaseBuilder().
public class AppLocalDb{
    static public AppLocalDbRepository db = Room.databaseBuilder(MyApplication.context,
            AppLocalDbRepository.class,
            "dbFileName.db").fallbackToDestructiveMigration().build();
}
