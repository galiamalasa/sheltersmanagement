package com.example.galia.sheltersmanagement.Model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.webkit.URLUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Galia on 11/07/2018.
 */

public class Model {

    public static Model instance = new Model();
    ModelFirebase modelFirebase;
    private Model(){
        modelFirebase = new ModelFirebase();
    }
    public void cancellGetAllShelters() {
        modelFirebase.cancellGetAllShelters();
    }


    class ShelterListData extends MutableLiveData<List<Shelter>> {
        @Override
        protected void onActive() {
            super.onActive();
            // new thread tasks
            // 1. get the shelters list from the local DB
            ShelterAsynchDao.getAll(new ShelterAsynchDao.ShelterAsynchDaoListener<List<Shelter>>() {
                @Override
                public void onComplete(List<Shelter> data) {
                    // 2. update the live data with the new shelter list
                    setValue(data);
                    Log.d("TAG","got shelters from local DB " + data.size());

                    // 3. get the shelter list from firebase
                    modelFirebase.getAllShelters(new ModelFirebase.GetAllSheltersListener() {
                        @Override
                        public void onSuccess(List<Shelter> shelterslist) {
                            // 4. update the live data with the new student list
                            setValue(shelterslist);
                            Log.d("TAG","got shelters from firebase " + shelterslist.size());

                            // 5. update the local DB
                            ShelterAsynchDao.insertAll(shelterslist, new ShelterAsynchDao.ShelterAsynchDaoListener<Boolean>() {
                                @Override
                                public void onComplete(Boolean data) {

                                }
                            });
                        }
                    });
                }
            });
        }

        @Override
        protected void onInactive() {
            super.onInactive();
            modelFirebase.cancellGetAllShelters();
            Log.d("TAG","cancellGetAllShelters");
        }

        public ShelterListData() {
            super();
            //setValue(AppLocalDb.db.shelterDao().getAll());
            setValue(new LinkedList<Shelter>());
        }
    }

    ShelterListData shelterListData = new ShelterListData();
    public LiveData<List<Shelter>> getAllShelters(){
        return shelterListData;
    }

    public void addShelter(Shelter sh){
        modelFirebase.addShelter(sh);
    }


    ////////////////////////////////////////////////////////
    //  HAndle Image Files
    ////////////////////////////////////////////////////////



    public interface SaveImageListener{
        void onDone(String url);
    }

    public void saveImage(Bitmap imageBitmap, SaveImageListener listener) {
        modelFirebase.saveImage(imageBitmap,listener);
    }

    public interface GetImageListener{
        void onDone(Bitmap imageBitmap);
    }


    public void getImage(final String url, final GetImageListener listener ){
       String localFileName = URLUtil.guessFileName(url, null, null);
        final Bitmap image = loadImageFromFile(localFileName);
        if (image == null) {                                      //if image not found - try downloading it from parse
            modelFirebase.getImage(url, new GetImageListener() {
                @Override
                public void onDone(Bitmap imageBitmap) {
                    if (imageBitmap == null) {
                        listener.onDone(null);
                    }else {
                        //2.  save the image localy
                        String localFileName = URLUtil.guessFileName(url, null, null);
                        Log.d("TAG", "save image to cache: " + localFileName);
                        saveImageToFile(imageBitmap, localFileName);
                        //3. return the image using the listener
                        listener.onDone(imageBitmap);
                    }
                }
            });
        }else {
            Log.d("TAG","OK reading cache image: " + localFileName);
            listener.onDone(image);
        }
    }

    // Store / Get from local mem
    private void saveImageToFile(Bitmap imageBitmap, String imageFileName){
        if (imageBitmap == null) return;
        try {
            File dir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            if (!dir.exists()) {
                dir.mkdir();
            }
            File imageFile = new File(dir,imageFileName);
            imageFile.createNewFile();

            OutputStream out = new FileOutputStream(imageFile);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();

            //addPicureToGallery(imageFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private Bitmap loadImageFromFile(String imageFileName){
        Bitmap bitmap = null;
        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File imageFile = new File(dir,imageFileName);
            InputStream inputStream = new FileInputStream(imageFile);
            bitmap = BitmapFactory.decodeStream(inputStream);
            Log.d("tag","got image from cache: " + imageFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

}
