package com.example.galia.sheltersmanagement.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Galia on 11/07/2018.
 */

public class ModelFirebase {

    public void addShelter(Shelter shelter) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("shelters").child(shelter.address).setValue(shelter);
        Log.d("TAG", "New shelter was added!! to firebase");
    }

    public void cancellGetAllShelters() {
        DatabaseReference stRef = FirebaseDatabase.getInstance().getReference().child("shelters");
        stRef.removeEventListener(eventListener);
    }


    interface GetAllSheltersListener {
        public void onSuccess(List<Shelter> shelterList);
    }

    ValueEventListener eventListener;


    public void getAllShelters(final GetAllSheltersListener listener) {
        DatabaseReference stRef = FirebaseDatabase.getInstance().getReference().child("shelters");

        eventListener = stRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Shelter> shList = new LinkedList<>();

                for (DataSnapshot stSnapshot : dataSnapshot.getChildren()) {
                    Shelter sh = stSnapshot.getValue(Shelter.class);
                    shList.add(sh);
                }
                listener.onSuccess(shList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    //Managing Files - Images
    public void saveImage(Bitmap imageBitmap, final Model.SaveImageListener listener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();

        Date d = new Date();
        String name = ""+ d.getTime();
        final StorageReference imagesRef = storage.getReference().child("images").child(name);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.onDone(null);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.onDone(downloadUrl.toString());
            }
        });

    }

    //Managing Files - Images
    public void getImage(String url, final Model.GetImageListener listener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference httpsReference = storage.getReferenceFromUrl(url);
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3 * ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                Log.d("TAG", "get image from firebase success");
                listener.onDone(image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAG", exception.getMessage());
                Log.d("TAG", "get image from firebase Failed");
                listener.onDone(null);
            }
        });
    }

}
