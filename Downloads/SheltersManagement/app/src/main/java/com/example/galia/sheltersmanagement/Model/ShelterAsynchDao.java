package com.example.galia.sheltersmanagement.Model;

import android.os.AsyncTask;

import java.util.List;

/**
 * Created by Galia on 11/07/2018.
 */

//This function is responsible for retriving asynchronilly  from database
public class ShelterAsynchDao {

    interface ShelterAsynchDaoListener<T>{
        void onComplete(T data);
    }
    static public void getAll(final ShelterAsynchDaoListener<List<Shelter>> listener) {
        class MyAsynchTask extends AsyncTask<String,String,List<Shelter>> {
            @Override
            protected List<Shelter> doInBackground(String... strings) {
                List<Shelter> shList = AppLocalDb.db.shelterDao().getAll();
                return shList;
            }

            @Override
            protected void onPostExecute(List<Shelter> shelters) {
                super.onPostExecute(shelters);
                listener.onComplete(shelters);
            }
        }
        MyAsynchTask task = new MyAsynchTask();
        task.execute();
    }


    static void insertAll(final List<Shelter> shelters, final ShelterAsynchDaoListener<Boolean> listener){
        class MyAsynchTask extends AsyncTask<List<Shelter>,String,Boolean>{
            @Override
            protected Boolean doInBackground(List<Shelter>... shelters) {
                for (Shelter sh:shelters[0]) {
                    AppLocalDb.db.shelterDao().insertAll(sh);
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);
                listener.onComplete(success);
            }
        }
        MyAsynchTask task = new MyAsynchTask();
        task.execute(shelters);
    }
}
