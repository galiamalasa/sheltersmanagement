package com.example.galia.sheltersmanagement.Model;

import java.util.List;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


/**
 * Created by Galia on 11/07/2018.
 */


//Contains the methods used for accessing the database - sqlite
@Dao
public interface ShelterDao {
    @Query("select * from Shelter")
    List<Shelter> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Shelter... shelters);

    @Delete
    void delete(Shelter shelter);
}